using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    public static GameObject Clone(Vector3 pos)
    {

        GameObject sphereClone = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        MeshRenderer rend = sphereClone.GetComponent<MeshRenderer>();
        rend.material.color = new Color(0, 0, 100);
        sphereClone.AddComponent<Rigidbody>();
        sphereClone.GetComponent<Rigidbody>().isKinematic = false;
        sphereClone.AddComponent<BoxCollider>();
        sphereClone.name = "CloneSphere";
        sphereClone.gameObject.SetActive(true);
        sphereClone.transform.position = pos;
        sphereClone.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

        return sphereClone;
    }

}