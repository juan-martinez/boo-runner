﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public float speed;
    public float increment;
    public float maxY;
    public float minY;

    private Vector2 targetPos;

    public int health;

    public GameObject moveEffect;
    public Animator camAnim;
    public Text healthDisplay;

    public GameObject spawner;
    public GameObject nameMenu;
    public GameObject moveButtons;



    private void Awake()
    {
        //InterfazUpdate.instance.score = 0;
        
        health = InterfazUpdate.Instance.health;


    }


    private void Update()
    {

        if (health <= 0) {


            SoundManager.PlaySound("GameOverSound");

            spawner.SetActive(false);
            moveButtons.SetActive(false);
            nameMenu.SetActive(true);
            Destroy(gameObject);
            

        }

        healthDisplay.text = health.ToString();

        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.UpArrow) && transform.position.y < maxY) {
            camAnim.SetTrigger("shake");
            Instantiate(moveEffect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y + increment);
        } else if (Input.GetKeyDown(KeyCode.DownArrow) && transform.position.y > minY) {
            camAnim.SetTrigger("shake");
            Instantiate(moveEffect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y - increment);
        }

    }

    public void MoveUp()
    {
        if(transform.position.y < maxY)
        {
            SoundManager.PlaySound("PlayerMoveSound");

            camAnim.SetTrigger("shake");
            Instantiate(moveEffect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y + increment);
 
        }


    }

    public void MoveDown()
    {
        if(transform.position.y > minY)
        {
            SoundManager.PlaySound("PlayerMoveSound");

            camAnim.SetTrigger("shake");
            Instantiate(moveEffect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y - increment);
        }
    }
}
