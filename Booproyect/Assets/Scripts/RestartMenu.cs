﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;      

public class RestartMenu : MonoBehaviour
{

    public void PlayButtonSound()
    {
        SoundManager.PlaySound("MenuButtonSound");
    }

    public void Restart()
    {

       
        SceneManager.LoadScene("Game");

        InterfazUpdate.Instance.health = 3;
        InterfazUpdate.Instance.score = 0;


    }

    public void ApplicationQuit()
    {
        InterfazUpdate.Instance.health = 3;
        InterfazUpdate.Instance.score = 0;
        //Debug.Log("QUIT GAME");
        Application.Quit();

    }

    public void MainMenu()
    {

        SceneManager.LoadScene("StartMenu");
        InterfazUpdate.Instance.StopMusic();
        InterfazUpdate.Instance.health = 3;
        InterfazUpdate.Instance.score = 0;
    }
}
