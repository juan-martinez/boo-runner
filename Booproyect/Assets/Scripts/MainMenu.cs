﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

   public void PlayButtonSound()
    {
        SoundManager.PlaySound("MenuButtonSound");
    }

   public void PlayGame()
    {


        InterfazUpdate.Instance.PlayMusic();
        SceneManager.LoadScene("Game");


    }

    public void Prototype()
    {


        InterfazUpdate.Instance.PlayMusic();
        SceneManager.LoadScene("Prototype");
    }

    public void ObjectPool()
    {


        InterfazUpdate.Instance.PlayMusic();
        SceneManager.LoadScene("ObjectPool");
    }

    public void ApplicationQuit()
    {

        

        //Debug.Log("QUIT GAME");
        Application.Quit();

    }
}
