using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using System;
using TMPro;


public class DBMaker : MonoBehaviour
{
    string ruteDB;
    string strConexion;
    string DBfile = "Juanma.sqlite";
    public TMPro.TextMeshProUGUI playerPos;

    IDbConnection dbConnection;
    IDbCommand dbComand;
    IDataReader reader;

    public List<string> rankings = new List<string>();

    private static DBMaker _instance;

    public static DBMaker Instance
    {
        get { return _instance; }

    }

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            // DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }

    public void ShowRanking()
    {

        GetRanking();

        CloseDB();


    }

    void CloseDB()
    {

        //cerrar

        dbComand.Dispose();
        dbComand = null;
        dbConnection.Close();
        dbConnection = null;
    }

    private void GetRanking()
    {

        OpenDB();

        dbComand = dbConnection.CreateCommand();

        string sqlQuery = "select * from Ranking order by score DESC limit 3";

        dbComand.CommandText = sqlQuery;

        reader = dbComand.ExecuteReader();


        int index = 1;

        while (reader.Read())
        {
            string name = reader["Name"].ToString();

            int score = Convert.ToInt32(reader["Score"]);
            if (index == 1)
            {
                SaveLoad.Instance.SaveGame(name, score);
            }
            playerPos.text = playerPos.text + "\n" + "  " + (index++).ToString() + " - " + name + " - " + score.ToString();

            
        }

        reader.Close();
        reader = null;

    }



    public void InsertScore(string name, float score)
    {
        
        OpenDB();
       
        dbComand = dbConnection.CreateCommand();

        string sqlQuery = "INSERT INTO Ranking (Name, Score) values( \"" + name + "\", \"" + score + "\")";
        
        dbComand.CommandText = sqlQuery;
    
        dbComand.ExecuteScalar();
       
        CloseDB();

      
    }








    void OpenDB()
    {

        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            ruteDB = Application.dataPath + "/StreamingAssets/" + DBfile;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {

            ruteDB = Application.persistentDataPath + "/" + DBfile;

            if (!File.Exists(ruteDB))
            {
                WWW loadDB = new WWW("jar;file://" + Application.dataPath + DBfile);

                while (!loadDB.isDone)
                {

                }
                File.WriteAllBytes(ruteDB, loadDB.bytes);
            }

        }

        // crear y abrir conexion
        ruteDB = Application.dataPath + "/StreamingAssets/" + DBfile;
        strConexion = "URI=file:" + ruteDB;
        dbConnection = new SqliteConnection(strConexion);

        dbConnection.Open();
        //CreateTable();
    }
}
