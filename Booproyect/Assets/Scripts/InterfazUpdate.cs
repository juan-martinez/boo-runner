using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InterfazUpdate : MonoBehaviour
{

    AudioSource _audioSource;
    public TMPro.TextMeshProUGUI maxScore;
 
    #region Score Script

     public float score;

    #endregion

    #region Player Script

    public int health;



    #endregion


    private static InterfazUpdate _instance;




    public static InterfazUpdate Instance
    {
        get { return _instance; }

    }

    void Awake()
    {

        if (_instance == null)
        {

            _instance = this;
            // DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this);
        }
    }


    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        if( maxScore)
        {
            string player = SaveLoad.Instance.LoadGame();
            maxScore.text = "Record:" + "\n" + player;
            Debug.Log("Hola");

        }

       

    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }

    public void PlayMusic()
    {
        _audioSource.Play();
    }




}
