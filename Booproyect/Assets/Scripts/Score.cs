﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

    public float score;
    public Text scoreDisplay;

    public bool isGame1;
    private int scoreForLoop;

    private void Awake()
    {
        //score = InterfazUpdate.Instance.score;
        score = 0;


    }

    private void Update()
    {
        InterfazUpdate.Instance.score = score;
        scoreDisplay.text = score.ToString();
        

        //if(scoreForLoop >= 30)
        //{
        //    if(isGame1 == true)
        //    {
        //        scoreForLoop = 0;
                
        //        SceneManager.LoadScene("Game 2");

        //    }else if(isGame1 == false)
        //    {
        //        scoreForLoop = 0;
        //        SceneManager.LoadScene("Game");
        //    }
        //}
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        

        score++;
        scoreForLoop++;
        Destroy(other.gameObject);
    }
}
