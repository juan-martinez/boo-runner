﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Security.AccessControl;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip playerMoveSound, playerHurtSound, menuButtonSound, gameOverSound;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        playerMoveSound = Resources.Load<AudioClip>("PlayerMoveSound");
        playerHurtSound = Resources.Load<AudioClip>("PlayerHurtSound");
        menuButtonSound = Resources.Load<AudioClip>("MenuButtonSound");
        gameOverSound = Resources.Load<AudioClip>("GameOverSound");

        audioSrc = GetComponent<AudioSource> ();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "PlayerMoveSound":
                audioSrc.PlayOneShot(playerMoveSound);
                break;
            case "PlayerHurtSound":
                audioSrc.PlayOneShot(playerHurtSound);
                break;
            case "MenuButtonSound":
                audioSrc.PlayOneShot(menuButtonSound);
                break;
            case "GameOverSound":
                audioSrc.PlayOneShot(gameOverSound);
                break;



        }
    }
}
