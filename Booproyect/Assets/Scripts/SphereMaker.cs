using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMaker : MonoBehaviour
{
    public float timeRespawn;

    private float randomX;
    //private float randomY;

    private void Start()
    {
        StartCoroutine(Respawn());
    }

    IEnumerator Respawn()
    {
        randomX = Random.Range(-8.00f, 8.00f);

        GameObject rainSphere = Sphere.Clone(new Vector3(randomX, 9, 0));


        rainSphere.transform.SetParent(gameObject.transform);

        yield return new WaitForSeconds(timeRespawn);


        Destroy(rainSphere, 5);
        StartCoroutine(Respawn());
    }
}